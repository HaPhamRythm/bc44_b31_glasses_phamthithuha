import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    console.log("test: ", this.props);
    let { data, handleWatchDetail } = this.props;
    let { image, name } = data; //this.props.data

    return (
      <div className="row">
        <div className="col-6 p-4">
          <div className="card text-left h-100">
            <img className="card-img-top" src={image} alt />
            <div className="card-body">
              <h4 className="card-title">{name}</h4>
            </div>
            <button
              onClick={() => {
                handleWatchDetail(data);
              }}
              className="btn btn-primary"
            >
              More
            </button>
            <button
              // onClick={() => {
              //   handleBuyShoe(data);
              // }}
              className="btn btn-primary"
            >
              Buy
            </button>
          </div>
        </div>
      </div>
    );
  }
}
