import React, { Component } from "react";

export default class DataBinding extends Component {
  // thuộc về class,đi kèm với this
  // url =
  //   "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWkBU5lBZRSx0m8dr7XzKB80MzufGiGs9ooQ&usqp=CAU";
  // renderAge = () => {
  //   return <h1>Age:2</h1>;
  // };

  url =
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWkBU5lBZRSx0m8dr7XzKB80MzufGiGs9ooQ&usqp=CAU";

  renderComment = () => {
    return <p>So yummy</p>;
  };
  render() {
    // thuộc function render, dùng từ khoá let
    // let username = "Alice";

    let food = "Salad";
    return (
      // <div>
      //   <h2>DataBinding</h2>
      //   <p>Username: {username}</p>
      //   <img src={this.urlSrc} alt="" />
      //   {this.renderAge()}
      // </div>

      <div>
        <img src={this.url} alt="" />;<p>Food: {food}</p>
        {this.renderComment()}
      </div>
    );
  }
}
