import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  // state = {
  //   username: "Alice",
  // };
  //state ở đâu thì setState ở đó
  // handleChange = () => {
  //   // toggle
  //   let name = this.state.username == "Alice" ? "Bob" : "Alice";
  //   this.setState({ username: name });
  // };

  state = {
    username: "Alice",
  };

  handleChangeName = () => {
    let name = this.state.username == "Alice" ? "Bob" : "Alice";
    this.setState({
      username: name,
    });
  };
  render() {
    return (
      // <div>
      //   <h2>DemoProps</h2>
      //   <UserInfo name={this.state.username} handleClick={this.handleChange} />
      // </div>

      <div>
        <h2>DemoProps</h2>
        <UserInfo
          name={this.state.username}
          changeName={this.handleChangeName}
        />
      </div>
    );
  }
}
