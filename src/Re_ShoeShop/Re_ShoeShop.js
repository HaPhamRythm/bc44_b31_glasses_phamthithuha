import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./data";

export default class Re_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detail: shoeArr[0],
  };

  handleClick = (selectedShoe) => {
    this.setState({
      detail: selectedShoe,
    });
  };
  render() {
    return (
      <div>
        <h1>ShoeShop</h1>
        <ListShoe
          listToRender={this.state.shoeArr}
          handleClickToListShoe={this.handleClick}
        />
        <DetailShoe detailOfSelectedShoe={this.state.detail} />
      </div>
    );
  }
}
