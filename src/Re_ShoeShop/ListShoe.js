import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderList = () => {
    return this.props.listToRender.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          data={item}
          handleCLickToItemShoe={this.props.handleClickToListShoe}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderList()}</div>;
  }
}
//{}
